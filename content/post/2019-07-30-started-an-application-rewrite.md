---
title: I Started an Application Rewrite
date: 2019-07-22
---

I started to rewrite my *first* application. The reason I'm rewriting it is to make it more modular and because I wanted to add support for arguments when you enter a command.

-----

## The Code

### Before Example
```python
def basic_calc():
	separator(1)

	print("Enter Number 1:")
	num1 = Decimal(float(input("--> ")))

	print("Enter Number 2:")
	num2 = Decimal(float(input("--> ")))

	separator(2)

	print("Addition = " + str(add(num1, num2)))
	print("Subtraction = " + str(sub(num1, num2)))
	print("Multiplication = " + str(mul(num1, num2)))
	print("Division = " + str(div(num1, num2)))
	separator(0)
```

### After Example
```python
import extra as e

def bc(args):
	try:
		if args[0] != '' and args[1] != "":
			num1 = e.Decimal(float(args[0]))
			num2 = e.Decimal(float(args[1]))
	except IndexError:
		e.separator(1)

		print("Enter Number 1:")
		num1 = e.Decimal(float(input("--> ")))

		print("Enter Number 2:")
		num2 = e.Decimal(float(input("--> ")))

	e.separator(2)

	print("Addition = " + str(e.add(num1, num2)))
	print("Subtraction = " + str(e.sub(num1, num2)))
	print("Multiplication = " + str(e.mul(num1, num2)))
	print("Division = " + str(e.div(num1, num2)))
	
	e.separator(0)
```
The code may seem more complicated, but now it and the function for every other command have their own file. The commands are mapped out in api.py
```python
import commands.basic_calc as basic_calc
import commands.tax_calc as tax_calc
import commands.compound_interest as compound_interest
import commands.commands as commands
import commands.credit_interest as credit_interest

commands = {
		"bc": basic_calc.bc,
		"tc": tax_calc.tc,
		"ci": compound_interest.ci,
		"ci2": credit_interest.ci2,
		"commands": commands.commands_list,
		"quit": quit
}

help_list = {
		"bc": "Basic Calculator",
		"tc": "Tax Calculator",
		"ci": "Compound Interest Calculator",
		"ci2": "Credit Interest Calculator",
		"commands": "Displays this list of commands",
		"quit": "Quit the application"
}
```
Then passed to the main execution when called by user input in main.py
```python
import api as a

if __name__ == '__main__':
	loop = True

	while loop:
		print("Enter a command: ")
		user_input = input("--> ")

		args = user_input.split()

		command = args[0]
		args.pop(0)

		try:
			func = a.commands[command]
			func(args)
		except KeyError:
			print("")
			if command == "":
				print("Not a valid command!")
			else:
				print(command + " is not a valid command!")
			print("")
```

----

## [Run the program to see it in action](https://repl.it/@DouglasFraser/Modular-CLI)