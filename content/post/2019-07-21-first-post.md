---
title: First Post!
date: 2019-07-21
---

## The Plan
The plan is to add things to this site at some point :P

{{< gallery caption-effect="fade" >}}
  {{< figure src="https://i.imgur.com/zPpzLtO.jpg" >}}
  {{< figure src="https://i.imgur.com/jEeB6z4.png" >}}
  {{< figure src="https://i.imgur.com/7SAo2Ht.png" >}}
{{< /gallery >}}
{{< load-photoswipe >}}